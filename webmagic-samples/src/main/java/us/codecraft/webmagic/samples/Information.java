package us.codecraft.webmagic.samples;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.model.annotation.ExtractBy;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author code4crafter@gmail.com
 */
public class Information {

    private String url;

    private String key;

    private Integer pageNumber;

    private Integer sizeCount;

    @ExtractBy("//td[@class='page1']/text()")
    private String pageInfo;

    @ExtractBy("//div[@class='page_main']")
    private String tBodyHtml;

    @Override
    public String toString() {
        return "Information{" +
                "key='" + key + '\'' +
                ", pageNumber=" + pageNumber +
                ", sizeCount=" + sizeCount +
                '}';
    }

    public static void main(String[] args) {
        String str = "第 1 页 共1页 每页 20 条记录 共 1 条记录 ";
        String str2 = "第 1 页 共10页 每页 20 条记录 仅展示前 200 条记录 共 1093 条记录";
        Pattern ptn = Pattern.compile("第 1 页 共(\\d+)页(.*)共 (\\d+) 条记录 ");
        Matcher matcher = ptn.matcher(str);
        if (matcher.matches()) {
            String page = matcher.group(1);
            String size = matcher.group(3);
            System.out.println(page + "-" + size);
        }
    }

    private Site site = Site.me().setCycleRetryTimes(5).setRetryTimes(5).setSleepTime(500).setTimeOut(3 * 60 * 1000)
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
            .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
            .addHeader("Accept-Language", "zh-CN,zh;q=0.9")
            .setCharset("UTF-8");

    public void run() {
        OOSpider ooSpider = OOSpider.create(site.setSleepTime(0), Information.class);
        //single download
        Information information = ooSpider.<Information>get(url);
        this.settBodyHtml(information.gettBodyHtml());
        String str = information.pageInfo;
        Pattern ptn = Pattern.compile("第 1 页 共(\\d+)页(.*)共 (\\d+) 条记录 ");
        Matcher matcher = ptn.matcher(str);
        if (matcher.matches()) {
            String page = matcher.group(1);
            String size = matcher.group(3);
            pageNumber = Integer.parseInt(page);
            sizeCount = Integer.parseInt(size);
        }
        ooSpider.close();
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(String pageInfo) {
        this.pageInfo = pageInfo;
    }

    public String gettBodyHtml() {
        return tBodyHtml;
    }

    public void settBodyHtml(String tBodyHtml) {
        this.tBodyHtml = tBodyHtml;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getSizeCount() {
        return sizeCount;
    }

    public void setSizeCount(Integer sizeCount) {
        this.sizeCount = sizeCount;
    }
}