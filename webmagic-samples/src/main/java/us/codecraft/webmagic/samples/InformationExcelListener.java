package us.codecraft.webmagic.samples;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhu.changgen
 * @Version 1.0
 * @Tate 2022/12/20 13:26
 * @Title excel读取
 */
public class InformationExcelListener extends AnalysisEventListener<InformationEnterprise> {

    private List<InformationEnterprise> list = new ArrayList<InformationEnterprise>();

    @Override
    public void invoke(InformationEnterprise o, AnalysisContext analysisContext) {
        list.add(o);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    public List<InformationEnterprise> getList() {
        return list;
    }
}
