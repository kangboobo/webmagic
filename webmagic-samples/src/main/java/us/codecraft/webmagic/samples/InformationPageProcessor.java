package us.codecraft.webmagic.samples;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.google.common.collect.Lists;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.selector.Html;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 410775541@qq.com <br>
 * @since 0.5.1
 */
public class InformationPageProcessor {


    static final String urlTemplate = "http://jzscyth.shaanxi.gov" +
            ".cn:7001/PDR/network/informationSearch/informationSearchList" +
            "?name" +
            "=&type=&certDeadline=&certReviewUnit=%s&regType=01&pid1=&pid2=&pid3=&pageNumber=%s&libraryName" +
            "=enterpriseLibrary";

    static final String detailUrlTemplate = "http://jzscyth.shaanxi.gov.cn:7001/PDR/network/Enterprise/Informations" +
            "/qyszit?enid=%s&name=&org_code=%s&type=";

    static final String filePath = "D:\\work\\self_code\\data.xlsx";

    static final String templatePath = "D:\\work\\self_code\\template.xlsx";


    public static void main(String[] args) throws Exception {

        Character start = new Character('A');
        List<InformationEnterprise> enterprises = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            char prefix = (char) (start + i);
            search(enterprises, String.valueOf(prefix));
        }
        System.out.println();
    }

    private static void search(List<InformationEnterprise> enterprises, String prefix) throws Exception {
        String selfUrl = String.format(urlTemplate, prefix, 1);
        Information information = new Information();
        information.setKey(prefix);
        information.setUrl(selfUrl);
        information.run();
        Thread.sleep(1000);
        System.out.println("information == " + information);
        if (information.getPageNumber() == null || information.getPageNumber() == 0) {
            return;
        }
        if (information.getPageNumber() < 10 || information.getSizeCount() <= 200) {
            // 第一页数据
            System.out.println("pageInfo [" + 1 + "] information == " + information);
            findData(information);
            // 查询第二页至第十页所有数据
            for (int i = 2; i <= information.getPageNumber(); i++) {
                try {
                    String url = String.format(urlTemplate, prefix, i);
                    information.setKey(prefix);
                    information.setUrl(url);
                    information.run();
                    System.out.println("pageInfo [" + i + "] information == " + information);
                    findData(information);
                    // Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        // 否则还需要递归
        for (int j = 0; j < 10; j++) {
            String key = prefix + j;
            try {
                search(enterprises, key);
            } catch (Exception e) {
                System.out.println("key search error:" + key);
                e.printStackTrace();
            }
        }
    }

    private static void findData(Information information) throws FileNotFoundException {
        try {
            //  System.out.println("findData start, html:" + information.gettBodyHtml());
            List<InformationEnterprise> enterprises = new ArrayList<>();
            if (information.gettBodyHtml() == null || information.gettBodyHtml().length() <= 0) {
                return;
            }
            Html bodyHtml = Html.create(information.gettBodyHtml());
            List<String> tableList = bodyHtml.xpath("//table[@class='tab_page']").all();
            if (tableList == null || tableList.size() < 2) {
                System.out.println("findData 页面数据异常, information:{} + information");
                return;
            }
            Html tableHtml = Html.create(tableList.get(1));
            Document document = tableHtml.getDocument();
            Elements tBody = document.getElementsByTag("tbody");
            if (tBody == null || tBody.size() == 0) {
                return;
            }
            Element tbElement = tBody.get(0);
            Elements trs = tbElement.getElementsByTag("tr");
            if (trs == null || trs.size() == 0) {
                return;
            }
            for (int i = 1; i < trs.size(); i++) {
                Element tr = trs.get(i);
                Elements tdList = tr.getElementsByTag("td");
                if (tdList == null || tdList.size() < 9) {
                    continue;
                }
                InformationEnterprise enterprise = new InformationEnterprise();
                enterprises.add(enterprise);

                Element nameTd = tdList.get(1);
                enterprise.name = nameTd.attr("title");

                Elements hrefId = nameTd.getElementsByTag("a");
                if (hrefId != null && hrefId.size() > 0) {
                    String onclick = hrefId.get(0).attr("onclick");
                    if (onclick != null) {
                        String[] strings = onclick.replace("'", "").replace(" ", "").split(",");
                        if (strings != null && strings.length >= 2) {
                            enterprise.enid = strings[1];
                            enterprise.orgCode = strings[2];
                            enterprise.detailUrl = String.format(detailUrlTemplate, enterprise.enid, enterprise.orgCode);
                        }
                    }
                }
                Element typeTd = tdList.get(3);
                enterprise.type = typeTd.text();

                Element addrTd = tdList.get(4);
                enterprise.address = addrTd.text();

                Element codeTd = tdList.get(5);
                enterprise.code = codeTd.text();

                Element expireTd = tdList.get(7);
                enterprise.expireDate = expireTd.text();

                Element rangeDescTd = tdList.get(9);
                enterprise.rangeDesc = rangeDescTd.attr("title");

                /*File file = new File(filePath);
                File template = new File(templatePath);
                OutputStream outputStream = new FileOutputStream(file);
                InputStream inputStream = new FileInputStream(template);

                ExcelWriter excelWriter = EasyExcel.write(outputStream).withTemplate(inputStream).build();
                WriteSheet writeSheet = EasyExcel.writerSheet().build();
                FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
                excelWriter.fill(enterprises, fillConfig, writeSheet);
                excelWriter.finish();*/
            }
            // 写入excel
            File file = new File(filePath);
            File template = new File(templatePath);
            if (file.exists()) {
                // 第二次按照原有格式，不需要表头，追加写入
                EasyExcel.write(file, InformationEnterprise.class).needHead(false).
                        withTemplate(file).file(template).sheet().doWrite(enterprises);
            } else {
                // 第一次写入需要表头
                EasyExcel.write(file, InformationEnterprise.class).sheet().doWrite(enterprises);
            }
            if (template.exists()) {
                file.delete();
                template.renameTo(file);
            }
        } catch (Exception e) {
            System.out.println("findData error, information:" + information + " error:" + e.getMessage());
            System.out.println("findData error, html: " + information.gettBodyHtml());
        }
    }


}
