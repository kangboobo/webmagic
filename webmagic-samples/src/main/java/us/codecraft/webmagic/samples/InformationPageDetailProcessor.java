package us.codecraft.webmagic.samples;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.metadata.ReadSheet;
import org.apache.poi.sl.usermodel.Sheet;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.selector.Html;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 410775541@qq.com <br>
 * @since 0.5.1
 */
public class InformationPageDetailProcessor {


    static final String detailUrlTemplate = "http://jzscyth.shaanxi.gov.cn:7001/PDR/network/Enterprise/Informations" +
            "/qyszit?enid=%s&name=&org_code=%s&type=";

    static final String dataPath = "D:\\work\\self_code\\data.xlsx";

    static final String filePath = "D:\\work\\self_code\\data_full.xlsx";

    static final String templatePath = "D:\\work\\self_code\\template_full.xlsx";

    private static Site site = Site.me().setCycleRetryTimes(5).setRetryTimes(5).setSleepTime(500).setTimeOut(3 * 60 * 1000)
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
            .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
            .addHeader("Accept-Language", "zh-CN,zh;q=0.9")
            .setCharset("UTF-8");


    public static void main(String[] args) throws Exception {
        // 读取excel
        InformationExcelListener excelListener = new InformationExcelListener();
        EasyExcel.read(dataPath, InformationEnterprise.class, excelListener).sheet().doRead();
        List<InformationEnterprise> list = excelListener.getList();
        int i=1;
        for (InformationEnterprise enterprise : list) {
            System.out.print("序号:" + i++ + " 企业：" + enterprise.name);
            if(enterprise.rangeDesc == null || !enterprise.rangeDesc.contains("一级")){
                System.out.println(" 非一级企业");
            }else {
                search(enterprise);
                System.out.println(" 一级企业，拉取基本信息完成");
            }
        }
        System.out.println("end !!!!!!!!!!!!");
    }

    private static void search(InformationEnterprise enterprise) throws Exception {
        try {
            OOSpider ooSpider = OOSpider.create(site.setSleepTime(0), InformationDetail.class);
            InformationDetail informationDetail = ooSpider.<InformationDetail>get(enterprise.detailUrl);
            ooSpider.close();
            String bodyHtmlStr = informationDetail.gettBodyHtml();
            if(bodyHtmlStr == null){
                return;
            }
            Html bodyHtml = Html.create(bodyHtmlStr);
            List<String> tableList = bodyHtml.xpath("//table[@class='detailTable']").all();
            if (tableList == null || tableList.size() <= 0) {
                System.out.println("search 页面数据不全, enterprise:" + enterprise.name);
                return;
            }
            Html tableHtml = Html.create(tableList.get(0));
            Document document = tableHtml.getDocument();
            Elements trs = document.getElementsByTag("tr");
            if(trs == null || trs.size() < 5){
                System.out.println("search 页面数据缺失, enterprise:" + enterprise.name);
                return;
            }
            // 营业执照号
            Element zhiZhao = trs.get(2);
            Elements zhiZhaoTds = zhiZhao.getElementsByTag("td");
            if(zhiZhaoTds != null && zhiZhaoTds.size() >= 4){
                enterprise.zhiZhao = zhiZhaoTds.get(3).text();
            }
            // 注册地址
            Element zhuCe = trs.get(3);
            Elements zhuCeTds = zhuCe.getElementsByTag("td");
            if(zhuCeTds != null && zhuCeTds.size() >= 4){
                enterprise.zhuCeAddr = zhuCeTds.get(1).text();
                enterprise.zhuCeZiBen = zhuCeTds.get(3).text();
            }
            // 组织机构代码
            Element zuZhi = trs.get(4);
            Elements zuZhiTds = zuZhi.getElementsByTag("td");
            if(zuZhiTds != null && zuZhiTds.size() >= 4){
                enterprise.zuZhiCode = zuZhiTds.get(1).text();
                enterprise.person = zuZhiTds.get(3).text();
            }
            // 写入excel
            File file = new File(filePath);
            File template = new File(templatePath);
            List<InformationEnterprise> enterprises = new ArrayList<>();
            enterprises.add(enterprise);
            if (file.exists()) {
                // 第二次按照原有格式，不需要表头，追加写入
                EasyExcel.write(file, InformationEnterprise.class).needHead(false).
                        withTemplate(file).file(template).sheet().doWrite(enterprises);
            } else {
                // 第一次写入需要表头
                EasyExcel.write(file, InformationEnterprise.class).sheet().doWrite(enterprises);
            }
            if (template.exists()) {
                file.delete();
                template.renameTo(file);
            }
        }catch (Exception e){
            System.out.println("search 页面数据异常, enterprise:" + enterprise.name);
            e.printStackTrace();
        }
    }

}
