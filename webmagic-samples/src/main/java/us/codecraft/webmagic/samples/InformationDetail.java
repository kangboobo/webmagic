package us.codecraft.webmagic.samples;

import us.codecraft.webmagic.model.annotation.ExtractBy;


/**
 * @author code4crafter@gmail.com
 */
public class InformationDetail {

    @ExtractBy("//div[@class='page_right']")
    private String tBodyHtml;

    public String gettBodyHtml() {
        return tBodyHtml;
    }

    public void settBodyHtml(String tBodyHtml) {
        this.tBodyHtml = tBodyHtml;
    }
}