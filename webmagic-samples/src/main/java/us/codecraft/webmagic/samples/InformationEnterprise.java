package us.codecraft.webmagic.samples;

import com.alibaba.excel.annotation.ExcelProperty;

public class InformationEnterprise{

    @ExcelProperty(value = "企业名称", index = 0)
    public String name;

    @ExcelProperty(value = "资质分类", index = 1)
    public String type;

    @ExcelProperty(value = "所在地", index = 2)
    public String address;

    @ExcelProperty(value = "资质证书号", index = 3)
    public String code;

    @ExcelProperty(value = "有效期至", index = 4)
    public String expireDate;

    @ExcelProperty(value = "资质范围", index = 5)
    public String rangeDesc;

    @ExcelProperty(value = "企业id", index = 6)
    public String enid;

    @ExcelProperty(value = "机构编码", index = 7)
    public String orgCode;

    @ExcelProperty(value = "详情页地址", index = 8)
    public String detailUrl;

    @ExcelProperty(value = "营业执照号", index = 9)
    public String zhiZhao;

    @ExcelProperty(value = "注册地址", index = 10)
    public String zhuCeAddr;

    @ExcelProperty(value = "注册资本金", index = 11)
    public String zhuCeZiBen;

    @ExcelProperty(value = "组织机构代码", index = 12)
    public String zuZhiCode;

    @ExcelProperty(value = "法定代表人", index = 13)
    public String person;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getRangeDesc() {
        return rangeDesc;
    }

    public void setRangeDesc(String rangeDesc) {
        this.rangeDesc = rangeDesc;
    }

    public String getEnid() {
        return enid;
    }

    public void setEnid(String enid) {
        this.enid = enid;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getZhiZhao() {
        return zhiZhao;
    }

    public void setZhiZhao(String zhiZhao) {
        this.zhiZhao = zhiZhao;
    }

    public String getZhuCeAddr() {
        return zhuCeAddr;
    }

    public void setZhuCeAddr(String zhuCeAddr) {
        this.zhuCeAddr = zhuCeAddr;
    }

    public String getZhuCeZiBen() {
        return zhuCeZiBen;
    }

    public void setZhuCeZiBen(String zhuCeZiBen) {
        this.zhuCeZiBen = zhuCeZiBen;
    }

    public String getZuZhiCode() {
        return zuZhiCode;
    }

    public void setZuZhiCode(String zuZhiCode) {
        this.zuZhiCode = zuZhiCode;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}
